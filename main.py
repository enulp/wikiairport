import urllib.request as urllib2
from bs4 import BeautifulSoup
import re

alphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"

def main():
    for i in alphabet:
        website = "https://en.wikipedia.org/wiki/List_of_airports_by_IATA_code:_" + i
        text = urllib2.urlopen(website).read()
        soup = BeautifulSoup(text)

        for link in soup.findAll('a'):
            tmp = link.get('href')
            if tmp and ("air" in tmp or "Air" in tmp) \
                    and not "IATA" in tmp \
                    and not "List" in tmp \
                    and not "Association" in tmp \
                    and not "Organization" in tmp \
                    and not "php" in tmp:
                print(tmp, end=', ')
                get_position(tmp)

def get_position(website):
        website = "https://en.wikipedia.org" + website
        text = urllib2.urlopen(website).read()
        soup = BeautifulSoup(text)
        for link in soup.findAll('span', {'class':'latitude'}):
            tmp = rmv(link)
            print (tmp, end=', ')
            break
        for link in soup.findAll('span', {'class':'longitude'}):
            tmp = rmv(link)
            print (tmp)
            break

def rmv(link):
    for i in link:
        return i

main()
